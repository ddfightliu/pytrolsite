const SEESION_CONFIG = {
  key: "koa:sess",
  maxAge: 7 * 24 * 60 * 60 * 1000,
  httpOnly: true,
  rolling: true,
  renew: true,
};
module.exports = SEESION_CONFIG;
