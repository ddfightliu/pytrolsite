const corsconf = {
  origin: (ctx) => {
    console.log(ctx.method, "request origin", ctx.origin);
    return "*";
  },
  maxAge: 5, //指定本次预检请求的有效期，单位为秒。
  credentials: true, //是否允许发送Cookie
  allowMethods: ["GET", "POST", "PUT", "DELETE", "OPTIONS"], //设置所允许的HTTP请求方法'
  allowHeaders: ["Content-Type", "Authorization", "Accept"], //设置服务器支持的所有头信息字段
  exposeHeaders: ["WWW-Authenticate", "Server-Authorization"], //设置获取其他自定义字段
};
module.exports = corsconf;
/*
function (ctx) {
    console.log(
      ctx.header.referer.substr(0, ctx.header.referer.length - 1),
      "cors"
    );
    return ctx.header.referer.substr(0, ctx.header.referer.length - 1);
  },*/
