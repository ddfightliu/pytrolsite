const Koa = require("koa");
const app = new Koa();
const views = require("koa-views");
const json = require("koa-json");
const onerror = require("koa-onerror");
const bodyparser = require("koa-bodyparser");
const logger = require("koa-logger");
const cors = require("koa2-cors");
const index = require("./routes/index");
const routes = require("./routes");
const users = require("./routes/users");
const confcors = require("./conf/cors");
const session = require("koa-session");
const SEESION_CONFIG = require("./conf/session");
// cookie
app.use(session(SEESION_CONFIG, app));
// error handler
onerror(app);

// middlewares
app.use(cors(confcors));

app.use(
  bodyparser({
    enableTypes: ["json", "form", "text"],
  })
);
app.use(json());
app.use(logger());
app.use(require("koa-static")(__dirname + "/public"));

app.use(
  views(__dirname + "/views", {
    extension: "pug",
  })
);

// logger
app.use(async (ctx, next) => {
  const start = new Date();
  await next();
  const ms = new Date() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
});

// routes
//app.use(index.routes(), index.allowedMethods());
//app.use(users.routes(), users.allowedMethods());
app.use(routes.routes(), users.allowedMethods());

// error-handling
app.on("error", (err, ctx) => {
  console.error("server error", err, ctx);
});

module.exports = app;
