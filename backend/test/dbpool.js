const db = require("../conf/db");
const mysql = require("mysql");
const oracledb = require("oracledb");

const mongodb = require("mongodb");
class mysqlpool {
  constructor() {
    this.pool = mysql.createPool(db.pytrol);
  }

  async query(sql) {
    if (sql && this.pool)
      return new Promise((resolve, reject) => {
        this.pool.getConnection((err, conn) => {
          if (err) {
            resolve(err);
          } else {
            conn.query(sql, async (err2, res) => {
              if (err2) {
                resolve({ errnum: err2.errorNum, message: err2.message });
              } else {
                resolve(JSON.stringify(res));
              }
              conn.release();
            });
          }
        });
      });
  }
}
class oraclepool {
  constructor() {
    oracledb.createPool(db.oracle, (err, pool) => {
      if (err) {
        this.poolerr = err;
      }
      if (pool) {
        this.pool = pool;
      }
    });
  }

  async query(sql) {
    if (sql && this.pool)
      return new Promise(async (resolve, reject) => {
        this.pool.getConnection((err, conn) => {
          if (err) {
            resolve(err);
          } else {
            conn.execute(sql, async (err2, res) => {
              if (err2) {
                resolve({ errnum: err2.errorNum, message: err2.message });
              } else {
                resolve(JSON.stringify(res));
              }
              conn.release();
            });
          }
        });
      });
  }
}
const mgcloudpool = new mongodb.MongoClient(db.mongodb.url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  serverApi: mongodb.ServerApiVersion.v1,
});
/*
    client.connect((err) => {
      const collection = client.db("test").collection("devices");
      // perform actions on the collection object
      client.close();
    });
*/
const pytrolpool = new mysqlpool();
const asspool = new oraclepool();

module.exports = { asspool, pytrolpool, mgcloudpool };
