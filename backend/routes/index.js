const { asspool, pytrolpool, mgcloudpool } = require("../test/dbpool");
const router = require("koa-router")();
const users = require("./users");
const db = require("./db");
const { aiwelllog } = require("../public/javascripts/aiwellLog");

router.get("/", async (ctx, next) => {
  await ctx.render("index", {
    title: "Hello Koa 2!",
  });
});
router.use(users.routes(), users.allowedMethods());
router.use(db.routes(), db.allowedMethods());

router.get("/string", async (ctx, next) => {
  ctx.body = "koa2 string";
});

router.get("/json", async (ctx, next) => {
  console.log("json");
  ctx.body = {
    title: "koa2 json",
  };
});

router.get("/get", async (ctx, next) => {
  ctx.body = { title: "get", data: "getget" };
});
router.post("/post", async (ctx, next) => {
  if (
    ctx.request.body.id == "ddfight" &&
    ctx.request.body.password == "123qweasdzxc"
  ) {
    ctx.body = "pass";
  } else {
    ctx.body = "forbidden";
  }
});

router.get("/dbpytrol", async (ctx, next) => {
  await pytrolpool.query(decodeURI(ctx.querystring)).then((resolve, reject) => {
    ctx.body = resolve;
  });
});
router.post("/dbpytrol", async (ctx, next) => {
  await pytrolpool.query(ctx.sql).then((resolve, reject) => {
    ctx.body = resolve;
  });
});
router.get("/dbass", async (ctx, next) => {
  await asspool.query(decodeURI(ctx.querystring)).then((resolve, reject) => {
    ctx.body = resolve;
  });
});
router.post("/dbass", async (ctx, next) => {
  await asspool.query(ctx.sql).then((resolve, reject) => {
    ctx.body = resolve;
  });
});

router.get("/api/aiwelllog", async (ctx, next) => {
  await aiwelllog(decodeURI(ctx.querystring)).then((resolve, reject) => {
    ctx.body = resolve;
  });
});

router.get("/dbmongocloud", async (ctx, next) => {
  mgcloudpool.connect((err, db) => {
    if (err) throw err;
    var whereStr = { name: "菜鸟教程" };
    db.db("runoob")
      .collection("site")
      .find(whereStr)
      .toArray((err, result) => {
        if (err) throw err;
        ctx.body = result;
        db.close();
      });
  });
});
router.post("/dbmongocloud", async (ctx, next) => {
  await pytrolpool.query(ctx.request.body.sql).then((resolve, reject) => {
    ctx.body = resolve;
  });
});
module.exports = router;
