const router = require("koa-router")();
const innerdb = require("./innerdb.js");
const { asspool, pytrolpool } = require("../../pool/dbpool");
router.prefix("/db");
router.post("/", async function (ctx, next) {
    if (ctx.request.body.coninfo == 'dbass') {
        await asspool.query(ctx.request.body.sql).then((resolve) => {
            ctx.body = resolve;
        }).catch((err) => { ctx.body = { err: err.errorNum, message: err.message }; })

    } else if (ctx.request.body.coninfo == 'dbpytrol') {
        await pytrolpool.query(ctx.request.body.sql).then((resolve) => {
            ctx.body = resolve;
        }).catch((err) => {
            ctx.body = { err: err.code, message: err.sqlMessage };
        })
        //dbpool.asspool.query()
    }
});
router.use(innerdb.routes(), innerdb.allowedMethods());
module.exports = router;
