const { oraclepool } = require("../../pool/dbpool");

async function aiwelllog(sql) {
  if (sql && oraclepool) {
    promise = new Promise(async (resolve, reject) => {
      oraclepool.getConnection((err, conn) => {
        if (err) {
          reject(err);
        } else {
          conn.execute(sql, async (err, res) => {
            if (err) {
              reject(err);
            } else {
              resolve(res);
            }
            conn.release();
          });
        }
      });
    });

    promise.res
  }
}
module.exports = { aiwelllog };
