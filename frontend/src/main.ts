import { createApp } from "vue";
import "element-plus/dist/index.css";
import "@surely-vue/table/dist/index.css";
import ElementPlus from "element-plus";
import App from "./App.vue";
import axios from "axios";
import vueaxios from "vue-axios";
import router from "@/router";
import { AgGridVue } from "ag-grid-vue3";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
//import Antd from "ant-design-vue";
//import "ant-design-vue/dist/antd.css";
import "ag-grid-enterprise";
import * as echarts from "echarts";
const app = createApp(App);
//app.provide(echarts, "echarts");
//app.config.globalProperties.$echarts = echarts;
app.provide("echarts", echarts);
app
  .use(vueaxios, axios)
  .use(ElementPlus)
  //.use(Antd)
  .use(router)
  .mount("#app");
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
app.component("AgGridVue", AgGridVue);
