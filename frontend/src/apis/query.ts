import axios from "axios";
import SERVERINFO from "../../conf/server.json";
import queryresnorm from "./queryresnorm";
/*
async function query(dbname, sql) {
  return await axios({
    method: "get",
    url: `http://10.76.180.223:3000/${dbname}?${sql}`,
  });
}
*/
export async function query(dbname: string, sql: string) {
  return await axios({
    method: "post",
    url:
      "http://" +
      SERVERINFO.ip +
      ":" +
      SERVERINFO.port.toString() +
      `/${dbname}`,
    data: { sql: sql },
  });
}

export async function dbquery(
  coninfo: { [connection_info: string]: unknown },
  sql: string
) {
  return await axios({
    method: "post",
    url: "http://" + SERVERINFO.ip + ":" + SERVERINFO.port.toString() + "/db",
    data: { coninfo: coninfo, sql: sql },
  });
}
//TODO:
export async function dataquery(
  coninfo: { [connection_info: string]: unknown },
  sql: string,
  target: string
) {
  const res = await axios({
    method: "post",
    url: "http://" + SERVERINFO.ip + ":" + SERVERINFO.port.toString() + "/db",
    data: { coninfo: coninfo, sql: sql },
  });
  if (coninfo.connection_info == "dbass") {
    const normres = queryresnorm(res, "dbass", target);
    return normres;
  }
  return res;
}
/*TODO: query url
 *  dffffdf
 */
