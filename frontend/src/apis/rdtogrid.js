//oracle rawdata transform to handsontable-grid-data(Array)
function rdtogrid(rawdata, colnamelist) {
  var output = [];
  var rec = {};
  for (let row of rawdata) {
    rec = {};
    row.forEach((element, index) => {
      rec[colnamelist[index]] = element;
      output.push(rec);
    });
  }
  return output;
}
export default rdtogrid;
