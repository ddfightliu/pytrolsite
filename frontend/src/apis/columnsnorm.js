import lodash from "lodash";
function columnsnorm(array, keys, coldefs = null) {
  /*
    array
    key
  */
  let res = [];
  for (let label in array) {
    var item = {};
    for (let key in keys) {
      item[keys[key]] = array[label];
    }
    if (coldefs && Object.keys(coldefs).includes(array[label])) {
      item = lodash.merge(item, coldefs[array[label]]);
    }
    res.push(item);
  }
  return res;
}
export default columnsnorm;
