function queryresnorm(rawdata, dbtype = "dbass", target = "table") {
  let normdata = null;
  if (dbtype == "dbass") {
    if (target == "table") {
      /*
       * rawdata = {metaData:Array[{name:String}],rows:Array[String]}
       * res = Array[{String:String}]
       */
      normdata = [];
      let colnames = [];
      rawdata[Object.keys(rawdata)[0]].forEach((value) => {
        colnames.push(Object.values(value)[0]);
      });
      rawdata[Object.keys(rawdata)[1]].forEach((value) => {
        let temp = {};
        for (let i = 0; i < value.length; i++) {
          temp[colnames[i]] = value[i];
        }
        normdata.push(temp);
      });
    } else if (target == "echarts") {
      /*
       * rawdata = {metaData:Array[{name:String}],rows:Array[String]}
       * res = {String:Array[]}
       */
      normdata = {};
      rawdata[Object.keys(rawdata)[0]].forEach((record) => {
        normdata[Object.values(record)[0]] = [];
      });
      rawdata[Object.keys(rawdata)[1]].forEach((record) => {
        record.forEach((item, index) => {
          normdata[rawdata[Object.keys(rawdata)[0]][index].name].push(item);
        });
      });
    } else if (target == "edtable") {
      normdata = [];
      let colnames = [];
      rawdata[Object.keys(rawdata)[0]].forEach((value) => {
        colnames.push(Object.values(value)[0]);
      });
      normdata.push(colnames);
      rawdata[Object.keys(rawdata)[1]].forEach((value) => {
        let temp = [];
        for (let i = 0; i < value.length; i++) {
          temp.push(value[i] ? value[i] : "");
        }
        normdata.push(temp);
      });
    }
  }
  console.log("rawData", rawdata);
  console.log("normfunction", normdata);
  return normdata;
}
export default queryresnorm;
