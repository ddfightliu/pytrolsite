import axios from "axios";
import SERVERINFO from "../../conf/server.json";
import { dbquery } from "./query";
/*
async function query(dbname, sql) {
  return await axios({
    method: "get",
    url: `http://10.76.180.223:3000/${dbname}?${sql}`,
  });
}
*/
export async function query(
  coninfo: { [connection_info: string]: unknown },
  sql: string
) {
  dbquery(coninfo, sql)
    .catch((err) => {
      return err;
    })
    .then((value) => {});
}

export async function dbquery(
  coninfo: { [connection_info: string]: unknown },
  sql: string
) {
  return await axios({
    method: "post",
    url: "http://" + SERVERINFO.ip + ":" + SERVERINFO.port.toString() + "/db",
    data: { coninfo: coninfo, sql: sql },
  });
}
/*TODO: query url
 *  dffffdf
 */
