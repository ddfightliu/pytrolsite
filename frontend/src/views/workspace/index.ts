import SqlQuery from "./SqlQuery.vue";
import UndulatingBlock from "./UndulatingBlock.vue";

export default {
  SqlQuery,
  UndulatingBlock,
};
