import { createRouter, createWebHistory } from "vue-router";
import LoginView from "@/views/LoginView.vue";
import MainView from "@/views/MainView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      // 登录
      path: "/",
      name: "home",
      component: LoginView,
    },
    {
      // 登录
      path: "/login",
      name: "loginview",
      component: LoginView,
    },
    {
      // 工作台
      path: "/main",
      name: "mainview",
      component: MainView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
  ],
});

export default router;
